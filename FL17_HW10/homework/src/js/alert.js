let alertMessage;

export function createAlert() {
    alertMessage = document.getElementById('alertMessageText');
    alertMessage.innerText = 'alert';
}

export function hideAlert() {
    alertMessage.hidden = true;
}

export function showAlert(alertText) {
    alertMessage.hidden = false;
    alertMessage.innerText = alertText;
    setTimeout(hideAlert, 2000);
}