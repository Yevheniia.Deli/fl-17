'use strict';

/**
 * Class
 * @constructor
 * @param size - size of pizza
 * @param type - type of pizza
 * @throws {PizzaException} - in case of improper use
 */
class Pizza {

    constructor(size, type) {
        this.size = size;
        this.type = type;
        this.extraIngredients = [];
        console.log("creating pizza: " + size + ", " + type);

        if (arguments.length !== 2) {

            throw new PizzaException("Required two arguments, given: " + arguments.length);

        }

        if (Pizza.allowedTypes.includes(type) === false) {
            throw new PizzaException("Invalid type");

        }

    }


    addExtraIngredient(ingredient) {
        console.log("addExtraIngredient: " + ingredient);


        if (Pizza.allowedIngredients.includes(ingredient) === false) {
            throw new PizzaException("Invalid ingredient");
        }

        if (this.extraIngredients.includes(ingredient) === true) {
            throw new PizzaException("Duplicate ingredient");
        }

        this.extraIngredients.push(ingredient);

        console.log("all ingredients: " + this.extraIngredients);


    }

    removeExtraIngredient(ingredient) {
        let index = this.extraIngredients.indexOf(ingredient);
        this.extraIngredients.splice(index, 1);
        console.log("removeExtraIngredient(ingredient) is called, ingredient:" + ingredient);
        console.log(this.extraIngredients);
    }

    getExtraIngredients() {
        return this.extraIngredients;
    }

    getSize() {
        return this.size;
    }


    getPrice() {
        let price = 0;

        if (this.size === Pizza.SIZE_L) {
            price += 100;
        }
        if (this.size === Pizza.SIZE_M) {
            price += 75;
        }
        if (this.size === Pizza.SIZE_S) {
            price += 50;
        }
        if (this.type === Pizza.TYPE_VEGGIE) {
            price += 50;
        }
        if (this.size === Pizza.TYPE_MARGHERITA) {
            price += 60;
        }
        if (this.size === Pizza.TYPE_PEPPERONI) {
            price += 70;
        }

        for (let i = 0; i < this.extraIngredients.length; i++) {
            let extraIngredient = this.extraIngredients[i];

            if (extraIngredient === Pizza.EXTRA_CHEESE) {
                price += 100;
            }
            if (extraIngredient === Pizza.EXTRA_TOMATOES) {
                price += 75;
            }
            if (extraIngredient === Pizza.EXTRA_MEAT) {
                price += 50;
            }
        }

        return price;
    }

    getPizzaInfo() {

        let info = "";
        info += "Size: " + this.getSize();
        info += "; type: " + this.type;
        info += "; extra ingredients: " + this.getExtraIngredients();
        info += "; price " + this.getPrice() + "UAH";

        return info;
    }

}


Pizza.SIZE_S = 'SMALL';
Pizza.SIZE_M = 'MEDIUM';
Pizza.SIZE_L = 'LARGE';

Pizza.TYPE_VEGGIE = "VEGGIE";
Pizza.TYPE_MARGHERITA = "MARGHERITA";
Pizza.TYPE_PEPPERONI = "PEPERONI";

Pizza.EXTRA_TOMATOES = 'TOMATOES';
Pizza.EXTRA_CHEESE = 'CHEESE';
Pizza.EXTRA_MEAT = 'MEAT';


Pizza.allowedSizes = [Pizza.SIZE_S, Pizza.SIZE_M, Pizza.SIZE_L];
Pizza.allowedTypes = [Pizza.TYPE_VEGGIE, Pizza.TYPE_MARGHERITA, Pizza.TYPE_PEPPERONI];
Pizza.allowedIngredients = [Pizza.EXTRA_TOMATOES, Pizza.EXTRA_CHEESE, Pizza.EXTRA_MEAT];


/**
 * Provides information about an error while working with a pizza.
 * details are stored in the log property.
 * @constructor
 */
class PizzaException {


    constructor(log) {
        this.log = log;
    }
}


