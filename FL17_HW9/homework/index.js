class Magazine {
    constructor() {
        this.articles = new Set();
        this.listOfFollowers = new Set();
        this.state = new ReadyForPushNotification();
    }

    notifyAllSubscribers() {
        let followers = this.listOfFollowers;
        for (const follower of followers) {
            let topics = follower.topics;
            for (const topic of topics) {
                for (const article of this.articles) {
                    if (article.topic === topic) {
                        follower.onUpdate(article.name + " " + follower.name);
                    }
                }
            }
        }
    }

}

class Article {

    constructor(name, topic) {
        this.name = name;
        this.topic = topic;
    }
}

class MagazineEmployee {

    constructor(name, role, magazine) {
        this.name = name;
        this.role = role;
        this.magazine = magazine;
    }

    addArticle(name) {
        let article = new Article(name, this.role);
        let articles = this.magazine.articles;
        articles.add(article);
        if (articles.size >= 5) {
            this.magazine.state = this.magazine.state.next();
        }
    }

    approve() {
        if (this.role !== 'manager') {
            console.log('you do not have permissions to do it');
            return;
        }

        if (this.magazine.state instanceof ReadyForPushNotification) {
            console.log(`Hello ${this.name}. You can't approve. We don't have enough of publications.`);
        } else if (this.magazine.state instanceof ReadyForApprove) {
            console.log(`Hello ${this.name} You've approved the changes`);
            this.magazine.state = this.magazine.state.next();
        } else if (this.magazine.state instanceof ReadyForPublish) {
            console.log(`Hello ${this.name} Publications have been already approved by you.`);
        } else if (this.magazine.state instanceof PublishInProgress) {
            console.log(`Hello ${this.name}. While we are publishing we can't do any actions`);
        }

    }

    publish() {
        let state = this.magazine.state;

        function resetState(magazine) {
            console.log("Ready For Push Notification");
            magazine.state = new ReadyForPushNotification();
        }

        if (state instanceof ReadyForPushNotification) {
            console.log(`Hello ${this.name}.  You can't publish. We are creating publications now.`)
        } else if (state instanceof ReadyForApprove) {
            console.log(`Hello ${this.name} You can't publish. We don't have a manager's approval.`)
        } else if (state instanceof ReadyForPublish) {
            console.log(`Hello ${this.name} You've recently published publications.`);
            this.magazine.state = this.magazine.state.next();
            let magazine = this.magazine;
            setTimeout(function () {
                resetState(magazine);
            }, 60000);
            this.magazine.notifyAllSubscribers();
        } else if (state instanceof PublishInProgress) {
            console.log(`Hello ${this.name}.  While we are publishing we can't do any actions.`)
        }
    }

}

class Follower {

    constructor(name) {
        this.name = name;
        this.topics = new Set();
    }

    subscribeTo(magazine, topic) {
        magazine.listOfFollowers.add(this);
        this.topics.add(topic);
    }

    unsubscribeTo(magazine, topic) {
        this.topics.delete(topic);
        if (this.topics.size === 0) {
            magazine.listOfFollowers.delete(this);
        }
    }

    onUpdate(data) {
        console.log(data);
    }
}


class MagazineState {
    constructor(name, nextState) {
        this.name = name;
        this.NextState = nextState;
    }

    next() {
        return new this.NextState();
    }
}

class ReadyForPushNotification extends MagazineState {
    constructor() {
        super('ReadyForPushNotification', ReadyForApprove);
    }
}

class ReadyForApprove extends MagazineState {
    constructor() {
        super('ReadyForApprove', ReadyForPublish);
    }
}

class ReadyForPublish extends MagazineState {
    constructor() {
        super('ReadyForPublish', PublishInProgress);
    }
}

class PublishInProgress extends MagazineState {
    constructor() {
        super('PublishInProgress', ReadyForPushNotification);
    }
}
