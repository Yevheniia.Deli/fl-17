let userName = '';

let socket = new WebSocket('ws://localhost:8080');


socket.onopen = function() {
    console.log('[open] Connection established');
};

socket.onmessage = function(event) {
    console.log(`[message] Data received from server: ${event.data}`);
    let chatElement = document.getElementById('chat');
    let chatMessage = JSON.parse(event.data);
    chatElement.innerHTML += `<p style="text-align: left">${chatMessage.name}</p>`;
    chatElement.innerHTML += `<p style="text-align: left; background: #1f7e9a; color: #23D4D4">${chatMessage.text}</p>`;
    chatElement.innerHTML += `<p style="text-align: left">${chatMessage.date}</p>`;
};


function handleOnClick() {
    if (userName === '') {
        userName = prompt('who are you?');
    }

    let text = prompt('What on your mind?');
    let chatMessage = new ChatMessage(userName, text, new Date());
    socket.send(JSON.stringify(chatMessage));
    let chatElement = document.getElementById('chat');
    chatElement.innerHTML += `<p style="text-align: right">${chatMessage.name}</p>`;
    chatElement.innerHTML += `<p style="text-align: right; background: #1f7e9a">${chatMessage.text}</p>`;
    chatElement.innerHTML += `<p style="text-align: right">${chatMessage.date}</p>`;
}


class ChatMessage {
    constructor(name, text, date) {
        this.name = name;
        this.text = text;
        this.date = date;
    }
}