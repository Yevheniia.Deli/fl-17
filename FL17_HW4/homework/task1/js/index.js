let users = [];

const spinner = document.getElementById('spinner');
const userList = document.getElementById('userList');
let request = new XMLHttpRequest();
request.onload = function () {
    showSpinner();
    let usersString = this.responseText;
    hideSpinner()
    users = JSON.parse(usersString);
    for (let i = 0; i < users.length; i++) {
        let user = users[i];
        let li = document.createElement('li');
        userList.appendChild(li);
        li.innerText = 'id: ' + user.id + '; name: ' + user.name;
        let editButton = document.createElement('button');
        userList.appendChild(editButton);
        editButton.innerText = 'edit';
        editButton.onclick = editButtonOnClick;
        editButton.id = user.id;

        let delButton = document.createElement('button');
        userList.appendChild(delButton);
        delButton.innerText = 'delete';
        delButton.onclick = delButtonOnClick;
        delButton.id = user.id;
    }

};
request.open('GET', 'https://jsonplaceholder.typicode.com/users');
request.send();

function editButtonOnClick() {
    let userId = parseInt(this.id);
    let user;

    for (let i = 0; i < users.length; i++) {
        user = users[i];
        if (userId === user.id) {
            console.log('found user: ' + userId);
            break;
        }
    }


    let editedName = prompt('edit user name', user.name);
    if (editedName === null) {
        return;
    }
    user.name = editedName;

    let updateRequest = new XMLHttpRequest();

    updateRequest.onload = function () {
        console.log('updated: ' + this.responseText);
    };

    updateRequest.open('PUT', 'https://jsonplaceholder.typicode.com/users/' + userId);
    updateRequest.send(user);
}

function delButtonOnClick() {
    let userId = this.id;

    let deleteRequest = new XMLHttpRequest();

    deleteRequest.onload = function () {
        let responseText = this.responseText;
        console.log('responseText: ' + responseText);
    };

    deleteRequest.open('DELETE',
        'https://jsonplaceholder.typicode.com/users/' + userId);
    deleteRequest.send();
}

function showSpinner() {
    spinner.className = 'show';
    setTimeout(() => {
        spinner.className = spinner.className.replace('show', '');
    }, 5000);
}
function hideSpinner() {
  spinner.className = spinner.className.replace('show', '');
}




