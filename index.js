const fs = require('fs');
const express = require('express');
const cors = require('cors');
const app = express();
const PORT = 8080;

app.use(express.json());
app.use(cors());
app.use(log);

app.get('/api/files', (req, res) => {
    fs.readdir('files/', (error, files) => {
        if (error) {
            res.status(500).send({
                message: 'Server error',
            });
        } else {
            res.status(200).send({
                message: 'Success',
                files,
            });
        }
    });
});
app.get('/api/files/:filename', validateFilename, (req, res) => {
    const { filename } = req.params;
    fs.stat('files/' + filename, (error, stats) => {
        if (error) {
            res.status(400).send({
                message: `No file with '${filename}' filename found`,
            });
        } else {
            fs.readFile('files/' + filename, 'utf8', function (error, data) {
                if (error) {
                    res.status(500).send({
                        message: 'Server error',
                    });
                } else {
                    res.status(200).send({
                        message: 'Success',
                        filename,
                        content: data,
                        extension: filename.split('.').pop(),
                        uploadedDate: stats.birthtime,
                    });
                }
            });
        }
    });
});
app.delete('/api/files/:filename', validateFilename, (req, res) => {
    const { filename } = req.params;
    fs.stat('files/' + filename, (error) => {
        if (error) {
            res.status(400).send({
                message: `No file with '${filename}' filename found`,
            });
        } else {
            fs.unlink('files/' + filename, function (error) {
                if (error) {
                    res.status(500).send({
                        message: 'Server error',
                    });
                } else {
                    res.status(200).send({
                        message: 'Success',
                    });
                }
            });
        }
    });
});
app.post('/api/files', validateFilename, (req, res) => {
    const { filename, content } = req.body;
    if (!content) {
        res.status(400).send({
            message: `Please specify 'content' parameter`,
        });
        return;
    }
    fs.writeFile('files/' + filename, content, (error) => {
        if (error) {
            res.status(500).send({
                message: 'Server error',
            });
        } else {
            res.status(200).send({
                message: 'File created successfully',
            });
        }
    });
});

function validateFilename(req, res, next) {
    const filename = req.params.filename || req.body.filename || '';
    const regExp = /\.(log|txt|json|yaml|xml|js)$/;
    if (regExp.test(filename)) {
        next();
    } else {
        res.status(400).send({
            message: `Client error`,
        });
    }
}

function log(req, res, next) {
    const log = [new Date().toLocaleString(), req.ip, req.originalUrl, req.method];
    fs.appendFile('logs.log', log.join(', ') + '\n', () => {});
    next();
}

app.listen(PORT, function () {
    console.log(`Web server listening on port ${PORT}`);
});
