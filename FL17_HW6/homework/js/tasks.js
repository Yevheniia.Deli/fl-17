const arr1 = ['1', '2', '3', '4', '5'];


function getMaxEvenElement(arr1) {
    let max = arr1.reduce(myFunction, -Number.MAX_VALUE);
    return max;

    function myFunction(max, value) {
        value = parseInt(value);
        if (value % 2 === 0) {
            return Math.max(value, max);
        }
        return max;
    }

}



let a = 3;
let b = 5;
[a, b] = [b, a];
console.log(a);
console.log(b);


function getValue(value) {
    return value ?? '-';
}




const arrayOfArrays = [
    ['name', 'dan'],
    ['age', '21'],
    ['city', 'Lviv']
]

function getObjFromArray(arrayOfArrays) {

    let obj = {};

    for (let i = 0; i < arrayOfArrays.length; i++) {
        let fieldArr = arrayOfArrays[i];
        let fieldName = fieldArr[0];
        let fieldValue = fieldArr[1];
        obj[fieldName] = fieldValue;
    }

    return obj;
}



const obj1 = {name: 'Nick'};
addUniqueId({name: 123});

function addUniqueId(obj) {
    let objWithId = {};
    Object.assign(objWithId, obj);

    objWithId['id'] = Symbol();
    return objWithId;
}




const oldObj = {
    name: 'Willow',
    details: {id: 1, age: 47, university: "LNU"}
}

const getRegroupedObject = (oldObj) => {
    let {
        name,
        details: {
            id,
            age,
            university
        }
    } = oldObj;
    return {
        university,
        user: {
            age,
            name,
            id
        }
    }
}


const arr = [2, 3, 4, 2, 4, "a", "c", "a"];

function getArrayWithUniqueElements(arr) {
    let a = new Set(arr);
    return a;
}


const phoneNumber = '0123456789';

function hideNumber(phoneNumber) {
    let last4 = phoneNumber.slice(6);
    return last4.padStart(10, '*');
}




function error() {
    throw new Error('b is required');
}

function add(a, b = error()) {
    return a + b;
}




function* generateIterableSequence() {
    yield 'I';
    yield 'love';
    yield 'Epam.'
}


const generatorObject = generateIterableSequence();

for (let value of generatorObject) {
    console.log(value);
}
