const root = document.getElementById('root');

let alertMessage = document.getElementById('alertMessageText');
alertMessage.innerText = 'alert';
alertMessage.hidden = true;

let addTweetButton = document.getElementsByClassName('addTweet')[0];
let textArea = document.getElementById('modifyItemInput');
let cancelButton = document.getElementById('cancelModification');

cancelButton.onclick = handleCancelButtonOnclick;

addTweetButton.onclick = addButtonOnclick;

let showLikedButton = document.createElement('button');
let backButton = document.createElement('button');
document.getElementById('navigationButtons').appendChild(showLikedButton);
document.getElementById('navigationButtons').appendChild(backButton);
backButton.hidden = true;
backButton.onclick = handleBackButtonOnclick;
backButton.innerText = 'back';

showLikedButton.innerText = 'Go to liked';
showLikedButton.onclick = handleShowLikedButtonOnClick;
showLikedButton.hidden = true;

let saveTweetButton = document.getElementById('saveModifiedItem');
saveTweetButton.onclick = saveTweetButtonOnClick;

showTweetList();

window.onhashchange = onHashChange;


function addButtonOnclick() {
    location.hash = '#/add';

    document.getElementById('tweetItems').hidden = true;
    document.getElementById('modifyItemHeader').innerText = 'Add tweet';
    document.getElementById('modifyItem').hidden = false;
}


function hasSameTweetInStorage(tweetText) {
    for (let i = 0; i < localStorage.length; i++) {
        let id = localStorage.key(i);
        let value = localStorage.getItem(id);

        let tweet = JSON.parse(value).tweet;
        if (tweet === tweetText) {
            return true;
        }
    }

    return false;
}

function hideAlert() {
    alertMessage.hidden = true;
}

function showAlert(alertText) {
    alertMessage.hidden = false;
    alertMessage.innerText = alertText;
    setTimeout(hideAlert, 2000);
}

function saveTweetButtonOnClick() {
    if (textArea.value.length === 0
        || textArea.value.length > 140
        || hasSameTweetInStorage(textArea.value)) {
        showAlert('Error! You can\'t tweet about that');
        return;
    }

    location.hash = '';

    let maxId = 0;
    for (let i = 0; i < localStorage.length; i++) {
        let id = parseInt(localStorage.key(i));
        if (id > maxId) {
            maxId = id;
        }
    }


    localStorage.setItem(String(maxId + 1), `{"tweet":"${textArea.value}","like":"false"}`);

    localStorage.removeItem(textArea.previousValue);
    textArea.value = '';

    hideEditSection();

    showTweetList();
}

function hideEditSection() {
    document.getElementById('modifyItem').hidden = true;
}


function showTweetList(likedOnly) {
    hideEditSection();

    document.getElementById('tweetItems').hidden = false;

    let tweetList = document.getElementById('list');
    tweetList.innerHTML = '';

    let hasLikes = false;
    showLikedButton.hidden = true;


    for (let i = 0; i < localStorage.length; i++) {
        let id = localStorage.key(i);
        let value = localStorage.getItem(id);

        let tweet = JSON.parse(value).tweet;
        let like = JSON.parse(value).like;

        if (likedOnly) {
            if (like !== true) {
                continue;
            }
        }

        let tweetListItem = document.createElement('li');
        tweetList.appendChild(tweetListItem);
        tweetListItem.innerText = tweet;
        tweetListItem.onclick = tweetListItemOnclick;
        tweetListItem.id = id;

        let span = document.createElement('span');
        tweetList.appendChild(span);

        let removeButton = document.createElement('button');
        span.appendChild(removeButton);
        removeButton.innerText = 'remove';
        removeButton.onclick = removeButtonOnclick;
        removeButton.id = id;

        let likeButton = document.createElement('button');
        span.appendChild(likeButton);
        likeButton.innerText = 'like';
        likeButton.onclick = handleLikeButtonOnclick;
        likeButton.id = id;

        if (like === true) {
            likeButton.innerText = 'unlike';
            hasLikes = true;
        }
    }

    if (!likedOnly && hasLikes) {
        showLikedButton.hidden = false;
    }

}


function tweetListItemOnclick() {
    let id = this.id;

    location.hash = '#/edit/:' + id;

    showEditPage(id);
}

function removeButtonOnclick() {
    localStorage.removeItem(this.id);
    showTweetList();
}

function handleLikeButtonOnclick() {
    let id = this.id;
    let value = localStorage.getItem(id);
    let tweetObject = JSON.parse(value);

    if (tweetObject.like === false) {
        tweetObject.like = true;
        showAlert(`Hooray! You liked tweet with id ${id}!`);
        localStorage.setItem(id, JSON.stringify(tweetObject));
    } else {
        tweetObject.like = false;
        showAlert(`Sorry you no longer like tweet with id ${id}`);
        localStorage.setItem(id, JSON.stringify(tweetObject));
    }
    showTweetList();
}


function handleShowLikedButtonOnClick() {
    location.hash = '#/liked';

    addTweetButton.hidden = true;
    showLikedButton.hidden = true;
    backButton.hidden = false;

    showTweetList(true);
}

function onHashChange() {

    if (location.hash === '#/add') {
        addButtonOnclick();
    } else if (location.hash === '#/liked') {
        handleShowLikedButtonOnClick();
    } else if (location.hash.startsWith('#/edit/')) {
        let id = location.hash.slice(8);
        showEditPage(id);
    } else {
        showTweetList();
    }
}


function showEditPage(id) {
    document.getElementById('modifyItem').hidden = false;
    document.getElementById('modifyItemHeader').innerText = 'Edit tweet';

    document.getElementById('tweetItems').hidden = true;

    let value = localStorage.getItem(id);
    let tweetText = JSON.parse(value).tweet;

    textArea.value = tweetText;
    textArea.previousValue = tweetText;
}


function handleCancelButtonOnclick() {
    location.hash = '';

    hideEditSection();
    showTweetList();
}


function handleBackButtonOnclick() {
    location.hash = '';

    hideEditSection();
    showTweetList();

    backButton.hidden = true;
    addTweetButton.hidden = false;
    showLikedButton.hidden = false;
}

