

function isEquals(arg1, arg2) {
    return arg1 === arg2;
}




function isBigger (arg1, arg2) {
    return arg1 > arg2;
}


function storeNames(...args) {
    let arr = [];
    for (let arg of args) {
        arr.push(arg);
    }

    return arr;
}


function getDifference(arg1, arg2) {

    if (arg2 > arg1) {
        let arg2Copy = arg2;
        arg2 = arg1;
        arg1 = arg2Copy;
    }

    return arg1 - arg2;

}


function negativeCount(numbersArr) {

    let negativeCount = 0;

    for (let number of numbersArr) {
        if (number < 0) {
            negativeCount++;
        }
    }

    return negativeCount;
}



function letterCount(arg1, arg2) {
    let count = 0;
    for (let arg1Element of arg1) {
        if (arg1Element === arg2) {
            count++;
        }

    }
    return count;
}


function countPoints(scoresArr) {
    const winPoints = 3;
    const drawPoints = 1;

    let results = 0;

    for (let element of scoresArr) {
        let pointsArr = element.split(':');
        let x = parseInt(pointsArr[0]);
        let y = parseInt(pointsArr[1]);
        if (x > y) {
            results += winPoints;
        }
        if (x === y) {
            results += drawPoints;
        }

    }
    return results;
}


