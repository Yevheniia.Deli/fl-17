let initialMaxPrize = 100;
let initialMaxNumber = 8;

let maxPrize = initialMaxPrize;
let maxNumber = initialMaxNumber;
let totalPrize = 0;

if (confirm('Do you want to play a game?') === false) {
    alert('You did not become a billionaire, but can.');
} else {
    playTheGame();
}


function playTheGame() {
    let winningNumber = generateWinningNumber();
    let isWon = false;

    for (let attempt = 1; attempt <= 3; attempt++) {
        let guess = askUserToGuessNumber(attempt);
        if (guess === winningNumber) {
            totalPrize += calculatePrize(attempt);
            isWon = true;
            break;
        }
    }

    if (!isWon) {
        alert('Thank you for your participation. Your prize is:' + totalPrize + '$');
        if (confirm('Do you want to play again')) {
            totalPrize = 0;
            maxPrize = initialMaxPrize;
            maxNumber = initialMaxNumber;
            playTheGame();
        } else {
        //    quit
        }
     }

    if (isWon) {
        let toContinue = confirm('Congratulation, you won! Your prize is:' + totalPrize + '$'
            + 'Do you want to continue?');
        if (toContinue === false) {
            alert('Thank you for your participation. Your prize is:' + totalPrize + '$');
            if (confirm('Do you want to play again')) {
                totalPrize = 0;
                maxPrize = initialMaxPrize;
                maxNumber = initialMaxNumber;
                playTheGame();
            } else {
            //    quit
            }
        }
        if (toContinue === true) {
            maxNumber += 4;
            maxPrize *= 2;
            playTheGame();
        }
    }
}

function generateWinningNumber() {
    return Math.floor(Math.random() * (maxNumber + 1));
}

function askUserToGuessNumber(attempt) {
    let message = 'Choose a roulette pocket number from 0 to ' + maxNumber;
    message += '\n' + 'Attempts left: ' + (3 - attempt + 1);
    message += '\n' + 'Total prize: ' + totalPrize + '$';
    message += '\n' + 'Possible prize on current attempt: ' + calculatePrize(attempt) + '$';

    return parseInt(prompt(message));
}

function calculatePrize(attempt) {
    if (attempt === 1) {
        return maxPrize;
    }
    if (attempt === 2) {
        return maxPrize / 2;
    }
    if (attempt === 3) {
        return maxPrize / 4;
    }
}
