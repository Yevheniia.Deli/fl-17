let profit = 0;
let totalAmount = 0;
let initialAmount = 0;
let numberOfYears = 0;
let percentageOfYear = 0;

do {
    initialAmount = parseFloat(prompt('Initial amount'));
} while (isValidInitialAmount() === false)

do {
    numberOfYears = parseInt(prompt('Number of years'));
} while (isValidYears() === false)

do {
    percentageOfYear = parseFloat(prompt('Percentage of a year'));
} while (isValidPercentage() === false)


calculateTotalAmount();
showResults();

function calculateTotalAmount() {
    totalAmount = initialAmount;
    for (let year = 1; year <= numberOfYears; year++) {
        let yearProfit = totalAmount * percentageOfYear / 100.0;
        totalAmount = totalAmount + yearProfit;
    }
    profit = totalAmount - initialAmount;

}


function showResults() {
    alert(
        'Initial amount: ' + initialAmount + '\n' +
        'Number of years: ' + numberOfYears + '\n' +
        'Percentage of year: ' + percentageOfYear + '\n' +
        '\n' +
        'Total profit: ' + profit.toFixed(2) + '\n' +
        'Total amount: ' + totalAmount.toFixed(2) + '\n'
    )

}


function isValidPercentage() {
    if (Number.isNaN(percentageOfYear)) {
        alert('Invalid input data');
        return false;
    }
    if (percentageOfYear > 100) {
        alert('Invalid input data');
        return false;
    }
    return true;
}

function isValidInitialAmount() {
    if (Number.isNaN(initialAmount)) {
        alert('Invalid input data');
        return false;
    }
    if (initialAmount < 1000) {
        alert('Invalid input data')
        return false;
    }
    return true;
}

function isValidYears() {
    if (Number.isNaN(numberOfYears)) {
        alert('Invalid input data');
        return false;
    }
    if (numberOfYears < 1) {
        alert('Invalid input data');
        return false;
    }
    if (Number.isInteger(numberOfYears) === false) {
        alert('Invalid input data');
        return false;
    }
    return true;
}