function visitLink(path) {
    let value = localStorage.getItem(path);
    if (value === null) {
        localStorage.setItem(path, String(1));
    } else {
        let count = Number(value);
        count = count + 1;
        localStorage.setItem(path, String(count));
    }

}




function viewResults() {
    let htmlAddition = '<ul>';
    for (let i = 0; i < localStorage.length; i++) {
        let key = localStorage.key(i);
        let value = localStorage.getItem(key);
        let result = 'You visited ' + key + ' ' + value + ' time(s)';
        htmlAddition += '<li>';
        htmlAddition += result;
        htmlAddition += '</li>';

    }
    htmlAddition += '</ul>';

    document.getElementById('content').innerHTML += htmlAddition;

    localStorage.clear();


}

viewResults();
