
const pipe = (value, ...funcs) => {

    try {
        for (let i = 0; i < funcs.length; i++) {
            let func = funcs[i];

            if (typeof func !== 'function') {
                throw 'Provided argument at position ' + i + ' is not a function!';
            }
        }
    } catch (e) {
        return e;
    }

    let result = value;
    for (let i = 0; i < funcs.length; i++) {
        let func = funcs[i];
        result = func(result);
    }

    return result;

};

