
const specialCellRow = 1;
const specialCellColumn = 1;

let cellsArray = new Array(3);

for (let i = 0; i < 3; i++) {
    cellsArray[i] = new Array(3);
}


createTable();

function createTable() {
    let root1 = document.getElementById('task1');
    let table = document.createElement('table');
    root1.appendChild(table);

    let tableBody = document.createElement('tbody');
    table.appendChild(tableBody);
    table.setAttribute('border', '2');


    for (let rowIndex = 0; rowIndex < 3; rowIndex++) {
        let row = document.createElement('tr');
        tableBody.appendChild(row);
        for (let columnIndex = 0; columnIndex < 3; columnIndex++) {
            let cell = document.createElement('td');
            cell.id = 'r' + rowIndex + 'c' + columnIndex;

            let rowArray = cellsArray[rowIndex];
            rowArray[columnIndex] = cell;

            row.appendChild(cell);
            cell.style.backgroundColor = 'white';

            if (rowIndex === specialCellRow && columnIndex === specialCellColumn) {
                cell.innerText = 'Special cell';
                cell.onclick = specialCellOnClick;
            } else if (columnIndex === 0) {
                cell.innerText = 'Cell';
                cell.onclick = cellFirstColumnOnClick;
            } else {
                cell.innerText = 'Cell';
                cell.onclick = cellOnClick;
            }
        }
    }
}

function cellOnClick() {
    this.style.backgroundColor = 'yellow';
}


function cellFirstColumnOnClick() {
    let rowIndex = this.id.slice(1, 2);
    let thisRowArray = cellsArray[rowIndex];
    let hasYellow = false;
    for (let i = 0; i < 3; i++) {
        let cell = thisRowArray[i];
        if (cell.style.backgroundColor === 'yellow') {
            hasYellow = true;
            break;
        }
    }

    if (hasYellow === false) {
        for (let i = 0; i < 3; i++) {
            let cell = cellsArray[rowIndex][i];
            cell.style.backgroundColor = 'blue';
        }
    }

}


function specialCellOnClick() {
    for (let rowIndex = 0; rowIndex < 3; rowIndex++) {
        let rowArray = cellsArray[rowIndex];
        for (let columnIndex = 0; columnIndex < 3; columnIndex++) {
            let cell = rowArray[columnIndex];

            if (cell.style.backgroundColor === 'white') {
                cell.style.backgroundColor = 'green';
            }
        }
    }
}


/* END TASK 1 */

/* START TASK 2: Your code goes here */

let root2 = document.getElementById('task2');
root2.style.marginTop = '50px';
root2.style.marginBottom = '50px';


let notificationBlock = document.createElement('div');
root2.appendChild(notificationBlock);
notificationBlock.innerText = 'Type number does not follow format +380*********';
notificationBlock.style.backgroundColor = 'red';
notificationBlock.hidden = true;

let input = document.createElement('input');
root2.appendChild(input);
input.placeholder = 'Type phone number in format +380*********';
input.style.width = '30%'
input.style.marginRight = '20px';




input.oninput = inputOnChange;

let sendButton = document.createElement('button');
root2.appendChild(sendButton);
sendButton.innerText = 'Send';
sendButton.onclick = onButtonClick;

function inputOnChange() {
    let phoneRegexInput = new RegExp('^\\+380\\d{9}$');

    let inputPhoneNumber = this.value;
    if (phoneRegexInput.test(inputPhoneNumber)) {
        sendButton.disabled = false;
        this.style.borderColor = 'black';
        notificationBlock.hidden = true;
    } else {
        sendButton.disabled = true;
        this.style.borderColor = 'red';
        notificationBlock.hidden = false;
        notificationBlock.style.backgroundColor = 'red';
        notificationBlock.style.width = '37%';
        notificationBlock.style.height = '30px';
        notificationBlock.style.marginBottom = '10px';
        notificationBlock.innerText = 'Type number does not follow format +380*********';
        notificationBlock.style.color = 'white';
    }

}


function onButtonClick() {
    notificationBlock.hidden = false;
    notificationBlock.style.backgroundColor = 'green';
    notificationBlock.style.width = '37%';
    notificationBlock.style.height = '30px';
    notificationBlock.style.marginTop = '10px';
    notificationBlock.innerText = 'Data was successfully sent';
    notificationBlock.style.color = 'white';
}




let clickX = 300;
let clickY = 165;
let ballX = 300;
let ballY = 165;
const ballStep = 10;

let leftZoneCenterX = 40;
let leftZoneCenterY = 165;

let rightZoneCenterX = 560;
let rightZoneCenterY = 165;

let leftScore = 0;
let rightScore = 0;


let canvas = document.createElement('canvas');
let root3 = document.getElementById('task3');
root3.addEventListener('ongoal', onGoal);


root3.appendChild(canvas);
canvas.id = 'canvas';
canvas.width = 600;
canvas.height = 330;
let ctx = canvas.getContext('2d');

let courtImage = new Image();

canvas.onclick = courtOnClick;

courtImage.src = 'assets/court.png';
courtImage.onload = function () {
    ctx.drawImage(courtImage, 0, 0, canvas.width, canvas.height);
};

let ballImage = new Image();
ballImage.src = 'assets/ball.png';
ballImage.style.zIndex = '1';
ballImage.style.transition = '1s all';

ballImage.onload = function () {
    ctx.drawImage(ballImage, ballX - 20, ballY - 20, 40, 40);
};

setInterval(redrawBall, 50);

let scoreBoard = document.createElement('div');
root3.appendChild(scoreBoard);
scoreBoard.style.width = '600px';
scoreBoard.style.height = '100px';
scoreBoard.style.fontSize = '100px';
scoreBoard.style.textAlign = 'center';
scoreBoard.innerText = '0:0';

let goalNotificationBlock = document.createElement('div');
root3.appendChild(goalNotificationBlock);
goalNotificationBlock.style.textAlign = 'center';
goalNotificationBlock.style.color = 'white';
goalNotificationBlock.style.width = '600px';


function redrawBall() {
    let ballStepX = (clickX - ballX) / ballStep;
    ballX += ballStepX;

    let ballStepY = (clickY - ballY) / ballStep;
    ballY += ballStepY;

    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(courtImage, 0, 0, canvas.width, canvas.height);
    ctx.drawImage(ballImage, ballX - 20, ballY - 20, 40, 40);

}


function courtOnClick(event) {
    clickX = event.offsetX;
    clickY = event.offsetY;

    if (isInsideLeftZone()) {
        const event = new CustomEvent('ongoal', {detail: 'Team B'});
        root3.dispatchEvent(event);

        // console.log("left goal!");
    }

    if (isInsideRightZone()) {
        const event = new CustomEvent('ongoal', {detail: 'Team A'});
        root3.dispatchEvent(event);
    }
}

function isInsideLeftZone() {
    return Math.abs(clickX - leftZoneCenterX) < 7.5 && Math.abs(clickY - leftZoneCenterY) < 7.5;
}

function isInsideRightZone() {
    return Math.abs(clickX - rightZoneCenterX) < 7.5 && Math.abs(clickY - rightZoneCenterY) < 7.5;
}

function onGoal(event) {

    let team = event.detail;
    if (team === 'Team B') {
        rightScore += 1;
    } else {
        leftScore += 1;
    }

    scoreBoard.innerText = leftScore + ':' + rightScore;
    goalNotificationBlock.innerText = team + ' scored!';
    goalNotificationBlock.style.color = 'black';

    setTimeout(hideNotification, 3000);
}

function hideNotification() {
    goalNotificationBlock.style.color = 'white';
}

/* END TASK 3 */
