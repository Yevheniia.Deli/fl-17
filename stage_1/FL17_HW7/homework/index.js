function getAge(birthday) {
    let today = new Date();
    let ageOnBirthdayThisYear = today.getFullYear() - birthday.getFullYear();

    let nowTimestamp = new Date().getTime();
    let thisYearBirthdayTimestamp = birthday.setFullYear(today.getFullYear());
    if (thisYearBirthdayTimestamp < nowTimestamp) {
        return ageOnBirthdayThisYear;
    } else {
        return ageOnBirthdayThisYear - 1;
    }

}



function getWeekDay(date) {

    let textDay = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    if (!(date instanceof Date)) {
        date = new Date(date);
    }

    let dayIndex = date.getDay();
    return textDay[dayIndex];
}


function getMillisInDay() {
    const millisInSec = 1000;
    const secondsInHour = 3600;
    const hoursInDay = 24;
    let millisInOneDay = millisInSec * secondsInHour * hoursInDay;
    return millisInOneDay;
}

function getAmountDaysToNewYear() {
    let nextYear = 1 + new Date().getFullYear();
    let newYearDate = new Date(nextYear, 0, 1);
    let newYearTimestamp = newYearDate.getTime();
    let nowTimestamp = Date.now();
    let amountMillisToNewYear = newYearTimestamp - nowTimestamp;
    let millisInOneDay = getMillisInDay();
    let daysToNewYear = amountMillisToNewYear / millisInOneDay;

    return daysToNewYear;
}


function getProgrammersDay(year) {
    const programmersDayNumber = 256;
    let programmerDayTimestamp = new Date(year, 0, 1).getTime() + (programmersDayNumber - 1) * getMillisInDay();
    let programmerDayDate = new Date(programmerDayTimestamp);
    let monthArr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let date = programmerDayDate.getDate();
    let month = monthArr[programmerDayDate.getMonth()];
    let programmerDayString = date + ' ' + month + ', ' + year + ' (' + getWeekDay(programmerDayDate) + ')';
    return programmerDayString
}


function howFarIs(weekDay) {

    let today = getWeekDay(new Date());
    if (weekDay.toLowerCase() === today.toLowerCase()) {
        return `Hey, today is ${today}`;
    } else {
        let textDay = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
        let weekDayIndex = textDay.indexOf(weekDay.toLowerCase());
        let todayIndex = textDay.indexOf(today.toLowerCase());
        let howFar = 0;
        if (todayIndex < weekDayIndex) {
            howFar = weekDayIndex - todayIndex;
        } else {
            const daysInWeek = 7;
            howFar = weekDayIndex - todayIndex + daysInWeek;
        }
        weekDay = weekDay[0].toUpperCase() + weekDay.slice(1).toLowerCase();
        return `It's ${howFar} day(s) left till ${weekDay}`
    }
}


function isValidIdentifier(myVar) {
    let reg = new RegExp('^[_$a-zA-Z][_$a-zA-Z0-9]*$');
    return reg.test(myVar);
}


function capitalize(str) {
    let capitalized = str.replace(new RegExp('\\b[a-zA-Z]', 'g'),
        function capitalizeWord(match) {
            return match.toUpperCase();
        });

    return capitalized;
}


function isValidAudioFile(filename) {
    let regExp = new RegExp('^[a-zA-Z]+(\\.mp3|\\.flac|\\.alac|\\.aac)$');
    return regExp.test(filename);
}


function getHexadecimalColors(str) {
    let regExp = new RegExp('#\\b[0-9a-fA-F]{3}\\b|#\\b[0-9a-fA-F]{6}\\b', 'g');
    let colorsArr = str.matchAll(regExp);
    let result = [];
    for (const colorsArrElement of colorsArr) {
        result.push(colorsArrElement[0]);
    }

    return result
}


function isValidPassword(str) {
    let regexp = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$');
    return regexp.test(str);
}


function addThousandsSeparators(numberOrString) {
    let string;
    if (typeof numberOrString === 'number') {
        string = String(numberOrString);
    } else {
        string = numberOrString;
    }

    let separated = string.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return separated
}



function getAllUrlsFromText(text) {
    let arr = [];
    let regExp = new RegExp(
        '(http|ftp|https):\\/\\/([\\w_-]+(?:(?:\\.[\\w_-]+)+))([\\w.,@?^=%&:\\/~+#-]*[\\w@?^=%&\\/~+#-])', 'g');
    let matchesArr = text.matchAll(regExp);

    for (const matchesArrElement of matchesArr) {
        arr.push(matchesArrElement[0]);
    }
    return arr
}