const appRoot = document.getElementById('app-root');

let countryArrow;
let areaArrow;
let sort;
let sortColumn;


const upArrow = '&#8593;';
const downArrow = '&#8595;';
const doubleArrow = '&#8597;';
let countrySort = 'unsorted';
let areaSort = 'unsorted';


let header = document.createElement('h1');
header.innerText = 'Countries Search';
appRoot.appendChild(header);

let searchForm = document.createElement('form');
appRoot.appendChild(searchForm);

let searchLabel = document.createElement('label');
searchForm.appendChild(searchLabel);
searchLabel.innerText = 'Please choose the type of search:'
searchLabel.htmlFor = 'typeOfSearch'

let radioDiv = document.createElement('div');
searchForm.appendChild(radioDiv);

let radioButtonTypeOfSearchByRegion = document.createElement('input');
radioButtonTypeOfSearchByRegion.type = 'radio';
radioButtonTypeOfSearchByRegion.id = 'region';
radioButtonTypeOfSearchByRegion.name = 'typeOfSearch'
radioButtonTypeOfSearchByRegion.value = 'byRegion';
radioButtonTypeOfSearchByRegion.onchange = radioButtonsOnChange
radioDiv.appendChild(radioButtonTypeOfSearchByRegion);

let label1 = document.createElement('label');
label1.htmlFor = 'region'
label1.innerText = 'By region'
radioDiv.appendChild(label1);


let radioButtonTypeOfSearchByLanguage = document.createElement('input');
radioButtonTypeOfSearchByLanguage.type = 'radio';
radioButtonTypeOfSearchByLanguage.id = 'language';
radioButtonTypeOfSearchByLanguage.name = 'typeOfSearch'
radioButtonTypeOfSearchByLanguage.value = 'byLanguage';
radioButtonTypeOfSearchByLanguage.onchange = radioButtonsOnChange
radioDiv.appendChild(radioButtonTypeOfSearchByLanguage);

let label2 = document.createElement('label');
label2.htmlFor = 'language';
label2.innerText = 'By language';
radioDiv.appendChild(label2);

let selectParagraph = document.createElement('p');
selectParagraph.innerText = 'Please choose search query:';

let selectDiv = document.createElement('div');
appRoot.appendChild(selectDiv);

let select = document.createElement('select');
select.id = 'selectAll'

let selectLabel = document.createElement('label');
selectDiv.appendChild(selectLabel);
selectLabel.innerText = 'Please choose search query:'
selectLabel.htmlFor = 'selectAll'

let defaultOption = document.createElement('option');
defaultOption.text = 'Select value';
select.add(defaultOption);

selectDiv.appendChild(select);

select.onchange = selectOnChange;

let tableResults = document.createElement('table');
appRoot.appendChild(tableResults);
let tableBody = document.createElement('tbody');

tableResults.appendChild(tableBody);
appRoot.appendChild(tableResults);
tableResults.setAttribute('border', '2');
generateTableHeader();


function generateTableHeader() {

    tableResults.hidden = true;

    let headerRow = document.createElement('tr');
    tableBody.appendChild(headerRow);
    let columnNames = ['Country name', 'Capital', 'World region', 'Languages', 'Area', 'Flag'];

    let tableHead;
    const languagesColumn = 4;
    for (let columnIndex = 0; columnIndex < columnNames.length; columnIndex++) {
        tableHead = document.createElement('th');
        headerRow.appendChild(tableHead);
        tableHead.innerText = columnNames[columnIndex];

        if (columnIndex === 0) {
            countryArrow = document.createElement('span');
            countryArrow.innerHTML = doubleArrow;
            countryArrow.onclick = countryArrowOnClick;
            tableHead.appendChild(countryArrow)
        }

        if (columnIndex === languagesColumn) {
            areaArrow = document.createElement('span');
            areaArrow.innerHTML = doubleArrow;
            areaArrow.onclick = areaArrowOnClick;
            tableHead.appendChild(areaArrow)
        }

    }

}


function countryArrowOnClick() {
    if (countrySort === 'unsorted') {
        countryArrow.innerHTML = upArrow;
        countrySort = 'up';
        sortTable(upArrow, 'country');
    } else if (countrySort === 'up') {
        countryArrow.innerHTML = downArrow;
        countrySort = 'down';
        sortTable(downArrow, 'country');
    } else if (countrySort === 'down') {
        countryArrow.innerHTML = doubleArrow
        countrySort = 'unsorted';
        sortTable(doubleArrow, 'country');
    }
}

function sortTable(direction, column) {
    sort = direction;
    sortColumn = column;
    selectOnChange();
}

function areaArrowOnClick() {
    if (areaSort === 'unsorted') {
        areaArrow.innerHTML = upArrow;
        areaSort = 'up';
        sortTable(upArrow, 'area');
    } else if (areaSort === 'up') {
        areaArrow.innerHTML = downArrow;
        areaSort = 'down';
        sortTable(downArrow, 'area');
    } else if (areaSort === 'down') {
        areaArrow.innerHTML = doubleArrow
        areaSort = 'unsorted';
        sortTable(doubleArrow, 'area');
    }
}

function radioButtonsOnChange() {
    while (select.options.length > 0) {
        select.remove(0);
    }

    let defaultOption = document.createElement('option');
    defaultOption.text = 'Select value';
    select.add(defaultOption);

    if (radioButtonTypeOfSearchByRegion.checked) {
        let regionsList = externalService.getRegionsList();
        for (let regionsListElement in regionsList) {
            let option = document.createElement('option');
            option.text = regionsList[regionsListElement];
            select.add(option);
        }
    } else if (radioButtonTypeOfSearchByLanguage.checked) {
        let languagesList = externalService.getLanguagesList();
        for (let element in languagesList) {
            let option = document.createElement('option');
            option.text = languagesList[element];
            select.add(option);
        }
    }
}

function clearTable() {
    let allRows = tableBody.getElementsByTagName('tr');
    console.log('allRows:');
    console.log(allRows);
    for (let i = allRows.length - 1; i >= 1 ; i--) {
        console.log(allRows[i]);
        allRows[i].remove();
    }
}

function selectOnChange() {
    clearTable();

    console.log('selectOnChange');
    if (select.value === 'Select value') {
        tableResults.hidden = true;
        return;
    }

    tableResults.hidden = false;

    let typeOfSearch;
    if (radioButtonTypeOfSearchByRegion.checked) {
        typeOfSearch = 'byRegion';
    } else {
        typeOfSearch = 'byLanguage';
    }

    loadTable(typeOfSearch, select.value);

}

function highlightRow() {
    this.style.backgroundColor = 'grey';
}

function unhighlightRow() {
    this.style.backgroundColor = null;
}

function loadTable(typeOfSearch, value) {
    let countryList;
    if (typeOfSearch === 'byRegion') {
        countryList = externalService.getCountryListByRegion(value);
    } else {
        countryList = externalService.getCountryListByLanguage(value);
    }

    if (sort === upArrow) {
        console.log('sort up');
        countryList = countryList.sort((a, b) => {
            if (sortColumn === 'country') {
                return a.name.localeCompare(b.name);
            } else {
                return a.area - b.area;
            }
        });

    } else if (sort === downArrow) {
        console.log('sort down');

        countryList = countryList.sort((a, b) => {
            if (sortColumn === 'country') {
                return -a.name.localeCompare(b.name);
            } else {
                return -(a.area - b.area);
            }
        });
    }


    for (let i = 0; i < countryList.length; i++) {

        let row = document.createElement('tr');

        row.onmouseover = highlightRow;
        row.onmouseout = unhighlightRow;

        tableBody.appendChild(row);
        for (let j = 0; j < 6; j++) {
            let cell = document.createElement('td');
            row.appendChild(cell);

            if (j === 0) {
                cell.innerText = countryList[i].name;
            }
            if (j === 1) {
                cell.innerText = countryList[i].capital;
            }
            if (j === 2) {
                cell.innerText = countryList[i].region;
            }
            if (j === 3) {
                let languagesObj = countryList[i].languages;
                let languagesString = '';
                for (let key in languagesObj) {
                    languagesString += ', ' + languagesObj[key];
                }

                cell.innerText = languagesString.slice(2);
            }
            if (j === 4) {
                cell.innerText = countryList[i].area;
            }
            if (j === 5) {
                let flagImage = document.createElement('img');
                cell.appendChild(flagImage);
                flagImage.src = countryList[i].flagURL;
            }
        }

    }

}