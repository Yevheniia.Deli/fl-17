function reverseNumber(num) {

    let sign = '+';
    if (num < 0) {
        sign = '-';
    }

    let numString = String(num);

    let reversed = '';
    for (let i = 1; i <= numString.length; i++) {
        reversed += numString[numString.length - i];
    }
    return parseInt(sign + reversed);
}


function forEach(arr, func) {
    for (let i = 0; i < arr.length; i++) {
        arr[i] = func(arr[i]);
    }
}


function map(arr, func) {
    let transformedArray = [];
    for (let arrElement of arr) {
        transformedArray.push(arrElement);
    }

    forEach(transformedArray, func);
    return transformedArray
}


function filter(arr, func) {
    let transformedArray = [];

    for (let arrElement of arr) {
        transformedArray.push(arrElement);
    }

    forEach(transformedArray, func);

    let filteredArray = [];
    for (let i = 0; i < transformedArray.length; i++) {
        if (transformedArray[i] === true) {
            filteredArray.push(arr[i]);
        }
    }

    return filteredArray;
}


function getAdultAppleLovers(data) {
    let adultAppleLovers = filter(data, function (person) {
        return person.age > 18 && person.favoriteFruit === 'apple'
    })

    return map(adultAppleLovers, function (person) {
        return person.name;
    });
}


function getKeys(obj) {
    let keys = [];
    for (let key in obj) {
        keys.push(key);
    }
    return keys;
}


function getValues(obj) {
    let values = [];
    for (let key in obj) {
        values.push(obj[key]);
    }
    return values;
}


function showFormattedDate(dateObj) {

    let dayNumber = dateObj.getDate();

    let monthArr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let month = monthArr[dateObj.getMonth()];
    let year = dateObj.getFullYear();

    return 'It is ' + dayNumber + ' of ' + month + ', ' + year;
}
