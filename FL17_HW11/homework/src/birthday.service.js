function getMillisInDay() {
    const millisInSec = 1000;
    const secondsInHour = 3600;
    const hoursInDay = 24;
    let millisInOneDay = millisInSec * secondsInHour * hoursInDay;
    return millisInOneDay;
}


const module2 = {
    congratulateWithBirthday() {
        this.myConsoleLog("Hooray!!! It is today!");
    },

    notifyWaitingTime(daysToBirthday) {
        daysToBirthday = Math.round(daysToBirthday);

        if (daysToBirthday > 0) {
            this.myConsoleLog("Soon...Please, wait just " + daysToBirthday + " day/days");
        } else {
            this.myConsoleLog("Oh, you have celebrated it " + -daysToBirthday + " day/s ago, don't you remember?");
        }
    },

    myConsoleLog(text) {
        console.log(text);
    },

    howLongToMyBirthday(dateOrTimestamp) {

        let birthdayTimestamp;
        if (dateOrTimestamp instanceof Date) {
            birthdayTimestamp = dateOrTimestamp.getTime();
        } else {
            if (isNaN(dateOrTimestamp)) {
                throw new Error("Wrong argument!");
            }
            if (dateOrTimestamp < 0 || dateOrTimestamp > new Date().getTime()) {
                throw new Error("Wrong argument!");
            }
            birthdayTimestamp = new Date(dateOrTimestamp).getTime();
        }

        let thisYear = new Date().getFullYear();
        let birthdayMonth = new Date(birthdayTimestamp).getMonth()
        let birthdayDay = new Date(birthdayTimestamp).getDate()

        let thisYearBirthdayDate = new Date(thisYear, birthdayMonth, birthdayDay);


        let nowTimestamp = Date.now();
        let amountMillisToBirthday = thisYearBirthdayDate.getTime() - nowTimestamp;
        let millisInOneDay = getMillisInDay();
        let daysToBirthday = amountMillisToBirthday / millisInOneDay;

        if (Math.abs(daysToBirthday) < 1) {
            this.congratulateWithBirthday();
        } else if (daysToBirthday < 365 / 2) {
            this.notifyWaitingTime(daysToBirthday);
        } else {
            this.notifyWaitingTime(-daysToBirthday);
        }
    }

}
module.exports = module2;

