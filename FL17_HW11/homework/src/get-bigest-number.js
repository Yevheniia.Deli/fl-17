const module1 = {

    getBigestNumber(...nums) {

        if (nums.length < 2) {
            throw 'Not enough arguments';
        }

        if (nums.length > 10) {
            throw 'Too many arguments';
        }

        let biggest = -Number.MAX_VALUE;

        for (const num of nums) {
            if (isNaN(num)) {
                throw 'Wrong argument type';
            }
            if (num > biggest) {
                biggest = num;
            }
        }

        return biggest;
    }


}


module.exports = module1;
