const myModule = require("../src/get-bigest-number");

describe('get biggest number', () => {
    it('should return biggest number', async () => {
        const result = myModule.getBigestNumber(1,1,1,1,10,1,1,1,1,1);
        expect(result).toBe(10);
    });

    it('should throw "Not enough arguments" if arguments number is less then 2' , async () => {
        expect( function(){ myModule.getBigestNumber(1); } ).toThrow("Not enough arguments");
    });


    it('should throw if arguments number is more then 10' , async () => {
        expect( function(){ myModule.getBigestNumber(1,1,1,1,1,10,1,1,1,1,1); } ).toThrow("Too many arguments");
    });

    it('should throw if one of arguments is NaN' , async () => {
        expect( function(){ myModule.getBigestNumber('blabla',1); } ).toThrow("Wrong argument type");
    });



});

//
