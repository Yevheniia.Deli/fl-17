const birthdayService = require("../src/birthday.service");

describe('Birthday Service', () => {
    it('should log `Soon...Please, wait just 2 day/days` if it will be in the next half year', async () => {
        let logSpy = spyOn(birthdayService, "myConsoleLog");

        birthdayService.howLongToMyBirthday(new Date(1981, 11, 21));
        expect(logSpy).toHaveBeenCalledWith("Soon...Please, wait just 2 day/days");
    });

    it('should log `Oh, you have celebrated it 3 day/s ago, don\'t you remember?` if it has already happened less than 6 month ago', async () => {
        let logSpy = spyOn(birthdayService, "myConsoleLog");

        birthdayService.howLongToMyBirthday(new Date(1981, 11, 16));
        expect(logSpy).toHaveBeenCalledWith("Oh, you have celebrated it 3 day/s ago, don't you remember?");
    });

    it('it should log ‘Hooray!!! It is today!’ if it is today', async () => {
        let logSpy = spyOn(birthdayService, "myConsoleLog");

        birthdayService.howLongToMyBirthday(new Date(1981, 11, 18));
        expect(logSpy).toHaveBeenCalledWith('Hooray!!! It is today!');
    });

    it('it should throw Error `Wrong argument!` in case of wrong argument', async () => {
        expect( function(){ birthdayService.howLongToMyBirthday('abc'); } )
            .toThrow(new Error("Wrong argument!"));
    });

    it('it should throw Error `Wrong argument!` in case of wrong timestamp argument', async () => {
        expect( function(){ birthdayService.howLongToMyBirthday(-1); } )
            .toThrow(new Error("Wrong argument!"));
    });

});
